// #include "virtualmem.h"

int fifo(const std::vector<int> & references, const int & n_frames) {
    int frames[n_frames], n_repl = 0, victim = 0;
    std::set<int> placed;
    memset(frames, -1, n_frames);
    for(int i=0; i<references.size(); i++) {
        if (placed.find(references[i]) != placed.end())
            continue;
        if (placed.find(frames[victim]) != placed.end()) {
            placed.erase(frames[victim]);
            n_repl++;
        }
        frames[victim] = references[i];
        placed.insert(references[i]);
        victim = (victim + 1) % n_frames;
    }
    // for(int i=0; i<n_frames; i++) std::cout << frames[i] << std::endl;
    return n_repl;
}
