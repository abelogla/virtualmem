
int lru_clock(const std::vector<int> & references, const int & n_frames) {
    int frames[n_frames];
    int n_reply = 0;
    int victim = 0;
    std::set<int> placed;
    std::map <int, int> markers;
    memset(frames, -1, n_frames);


    for(int i=0; i<references.size(); i++) {
        if (markers.find(references[i]) != markers.end()){  //found it
        	markers[references[i]] = 1;
        	continue;
        }
        if (placed.find(frames[victim]) != placed.end()) {
        	auto search = markers.find(frames[victim]);
        	if(search->second == 0){
        		placed.erase(frames[victim]);
        		n_reply++;
        	}
        	while((victim + 1)%n_frames){
        		auto search = markers.find(frames[victim]);
        		if(search->second == 1){
        			markers[frames[victim]]  = 0;   			
        		}
        		else{
        			placed.erase(frames[victim]);
        			placed.insert(references[i]);
        			frames[victim] = references[i];
        			n_reply++;
        			break;
        		}
        	}
        }
        frames[victim] = references[i];
    	placed.insert(references[i]);
    	victim = (victim + 1) % n_frames;
    }
    return n_reply;
}
