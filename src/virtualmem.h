#ifndef VIRTUALMEM_H
#define VIRTUALMEM_H

#include <iostream>
#include <algorithm>
#include <chrono>
#include <vector>
#include <sstream>
#include <fstream>
#include <cstring>
#include <cmath>
#include <iomanip>
#include <queue>
#include <map>
#include <set>
#include "optimal.cpp"
#include "fifo.cpp"
#include "lfu.cpp"
#include "lru_clock.cpp"
#include "lru_ref8.cpp"
#include "lru_stack.cpp"

int n_frames = 5;
std::vector<int> references;
std::string input_file, policy_s = "FIFO";
int (*selected_algo)(const std::vector<int> &, const int &) = &fifo;

int optimal(const std::vector<int> &, const int &);
int fifo(const std::vector<int> &, const int &);
int lfu(const std::vector<int> &, const int &);
int lru_stack(const std::vector<int> &, const int &);
int lru_clock(const std::vector<int> &, const int &);
int lru_ref8(const std::vector<int> &, const int &);
void parse_args(int, char *);
void print_usage(const char *);
void pr_error(const char *);
void get_ref_sequence();
void run_algo(int (*algo)(const std::vector<int> &, const int &), double &, int &);


#endif
