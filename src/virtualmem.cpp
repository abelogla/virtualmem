#include "virtualmem.h"

void print_usage(const char * exec) {
    std::cout   << "\nUSAGE: " << exec << " [Options]\n\n"
                << "Options:\n" << "\t-h\t\t\t\tPrint a usage summary\n"
                << "\t-f <available-frames>\t\tSet the number of available frames. Default: 5\n"
                << "\t-r <replacement-policy>\t\tSet the page replacement policy. Available:\n"
                << "\t\t\t\t\t - FIFO (First-in-First-out)\t*Default*\n"
                << "\t\t\t\t\t - LFU (Least-frequently-used)\n"
                << "\t\t\t\t\t - LRU-STACK (Least-recently-used stack implementation)\n"
                << "\t\t\t\t\t - LRU-CLOCK (Least-recently-used clock implementation - second chance alg.)\n"
                << "\t\t\t\t\t - LRU-REF8 (Least-recently-used Reference-bit implementation, using 8 reference bit)\n"
                << "\t-i <input-file>\t\t\tRead the page reference sequence from a specified file. If not given\n"
                << "\t\t\t\t\tthe sequence is read from STDIN ended with ENTER\n";
    exit(0);
}

void parse_args(int ac, char * av[]) {
    const char * exec_name = av[0];
    for(int i=1; i<ac; i++) {
        std::string current = av[i];
        if (current[0] == '-') {
            switch (current[1]) {
                case 'h':
                print_usage(exec_name);
                case 'f':
                if (++i >= ac) print_usage(exec_name);
                try {
                    n_frames = std::stoi(av[i]);
                }
                catch(std::invalid_argument e) {
                    print_usage(exec_name);
                }
                break;
                case 'r':
                if (++i >= ac) print_usage(exec_name);
                policy_s = av[i];
                if (strcasestr(policy_s.c_str(), "FIFO"))
                    selected_algo = &fifo;
                else if (strcasestr(policy_s.c_str(), "LFU"))
                    selected_algo = &lfu;
                else if (strcasestr(policy_s.c_str(), "LRU-STACK"))
                    selected_algo = &lru_stack;
                else if (strcasestr(policy_s.c_str(), "LRU-CLOCK"))
                    selected_algo = &lru_clock;
                else if (strcasestr(policy_s.c_str(), "LRU-REF8"))
                    selected_algo = &lru_ref8;
                else print_usage(exec_name);
                break;
                case 'i':
                if (++i >= ac) print_usage(exec_name);
                input_file = av[i];
                break;
                default:
                print_usage(exec_name);
            }
        }
        else print_usage(exec_name);
    }
}

void pr_error(const char * msg) {
    perror(msg);
    exit(1);
}

void get_ref_sequence() {
    std::string ref;
    std::stringstream instring;
    if (!input_file.empty()) {
        std::ifstream f;
        f.open(input_file);
        if(!f.is_open())
            pr_error("cannot open file");
        instring << f.rdbuf();
        f.close();
    }
    else {
        std::cin.clear();
        std::cout << "Enter page reference sequence: ";
        std::getline(std::cin, ref);
        instring << ref;
    }
    while (instring >> ref) {
        try {
            references.push_back(std::stoi(ref));
        }
        catch(std::invalid_argument e) {
            pr_error("error while parsing input");
        }
    }
}

void run_algo(int (*algo)(const std::vector<int> &, const int &), double & elapsed_t, int & n_repl) {
    auto starttime = std::chrono::steady_clock::now();
    n_repl = (*algo)(references, n_frames);
    auto end = std::chrono::steady_clock::now();
    elapsed_t = (double)std::chrono::duration_cast<std::chrono::microseconds>(end - starttime).count()/1000;
}

int main(int argc, char *argv[]) {
    int sel_algo_repl = 1, optimal_repl = 1;
    double sel_algo_time = 1, optimal_time = 1;
    parse_args(argc, argv);
    get_ref_sequence();
    if (!references.size()) pr_error("empty input");
    run_algo(selected_algo, sel_algo_time, sel_algo_repl);
    run_algo(&optimal, optimal_time, optimal_repl);
    double diff = ((sel_algo_time - optimal_time)/optimal_time)*100;
    double penalty = double(sel_algo_repl - optimal_repl)/(optimal_repl)*100;
    std::cout.setf(std::ios::left);
    std::cout   << "\n" << std::setw(55) << "# of page replacements with " + policy_s << ": " << sel_algo_repl << std::endl
                << std::setw(55) << "# of page replacements with Optimal" << ": " << optimal_repl << std::endl
                << std::setw(55) << "\% page replacement penalty using " + policy_s << ": "
                << ((optimal_repl != 0) ? penalty : 0) << "\%\n\n"
                << std::setw(55) << "Total time to run " + policy_s + " algorithm" << ": " << sel_algo_time << " msec\n"
                << std::setw(55) << "Total time to run Optimal algorithm" << ": " << optimal_time << " msec\n"
                << policy_s << " is " << std::abs(diff) << "\% " << ((diff < 0) ? "faster" : "slower")
                << " than Optimal algorithm\n\n";
    return 0;
}
