
int lru_ref8(const std::vector<int> & references, const int & n_frames) {
    std::map<int, int> pages;
    int n_repl = 0;
    for(int i=0; i<references.size(); i++) {
        for(std::map<int,int>::iterator it=pages.begin(); it != pages.end(); it++)
            it->second >>= 1;
        if(pages.size() != n_frames) {
            if (pages.find(references[i]) != pages.end()) {
                pages.find(references[i])->second += 0x80;
            }
            else {
                pages[references[i]] = 0x80;
            }
        }
        else {
            if (pages.find(references[i]) != pages.end()) {
                pages.find(references[i])->second += 0x80;
            }
            else {
                int victim, count = 0x80;
                for(std::pair<int, int> p : pages) {
                    if (p.second < count) {
                        count = p.second;
                        victim = p.first;
                    }
                }
                pages.erase(victim);
                pages[references[i]] = 0x80;
                n_repl++;
            }
        }
    }
    return n_repl;
}
