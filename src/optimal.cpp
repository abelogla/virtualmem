
int optimal(const std::vector<int> & references, const int & n_frames) {
    int frames[n_frames], n_repl = 0, cur_pos = 0;
    std::map<int, int> position, order;
    for(int i=0, j=0; j<n_frames && i<references.size(); i++) {
        if (position.find(references[i]) == position.end()) {
            frames[j] = references[i];
            position[references[i]] = j;
            order[references[i]] = i;
            j++;
        }
        cur_pos = i + 1;
    }
    for(int i=cur_pos; i<references.size(); i++) {
        if (position.find(references[i]) != position.end()) {
            order[references[i]] = i;
            continue;
        }
        std::pair<int, int> victim;
        std::vector<int> victims;
        for(int j=0; j<n_frames; j++) {
            if(i+1 == references.size()) {
                victims.push_back(frames[j]);
                continue;
            }
            for(int k=i+1; k<references.size(); k++) {
                if(frames[j] == references[k]) {
                    if(victim.second < k) {
                        victim.first = frames[j];
                        victim.second = k;
                    }
                    break;
                }
                if(k+1 == references.size()) victims.push_back(frames[j]);
            }
        }
        if (victims.size()) {
            int pos = i;
            for (int v=0; v < victims.size(); v++) {
                if(pos > order[victims[v]]) {
                    pos = order[victims[v]];
                    victim.first = victims[v];
                }
            }
        }
        n_repl++;
        frames[position.find(victim.first)->second] = references[i];
        position[references[i]] = position.find(victim.first)->second;
        order[references[i]] = i;
        order.erase(victim.first);
        position.erase(victim.first);
    }
    // for(int i=0; i<n_frames; i++) std::cout << frames[i] << std::endl;
    return n_repl;
}
