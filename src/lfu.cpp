
int lfu(const std::vector<int> & references, const int & n_frames) {
    int frames[n_frames];
    std::set<int> placed;
    int n_repl = 0;
    int count = 0;
    int tempvalue = 0;
    std::queue<int> tempq;
    int tempfreq = 2147483647;
    std::map<int, int> frequency;
    memset(frames, -1, n_frames);

    for(int i = 0; i<references.size(); i++){
    	if(placed.find(references[i]) != placed.end()){
    		frequency[references[i]]++;
    		continue;
    	}
    	if (placed.find(frames[count]) != placed.end()) {
    		for(auto const it : frequency){
    			if(it.second > 0 && it.second <= tempfreq){
    				tempvalue = it.first;
    				tempfreq = it.second;
    				tempq.push(tempvalue);
    			}
    		}
    		placed.erase(frames[count]);
    		placed.insert(tempq.front());
            //frequency[tempq.front()] = 0;
            frames[count] = tempq.front();
            count = (count + 1) % n_frames;
    		n_repl++;

    		while(!tempq.empty())
    			tempq.pop();
            
        continue;	
    	}
        frames[count] = references[i];
        placed.insert(references[i]);
        frequency[references[i]]++;
        count = (count + 1) % n_frames;
    }
    return n_repl;
}