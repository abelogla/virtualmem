
int lru_stack(const std::vector<int> & references, const int & n_frames) {
	int n_repl = 0;
	std::set<int> placed;
    std::vector<int> frames;
    for(int i=0; i<references.size(); i++) {
        if (placed.find(references[i]) != placed.end()) {
            std::vector<int>::iterator pos = std::find(frames.begin(), frames.end(), references[i]);
            frames.erase(pos);
            frames.push_back(references[i]);
        }
        else {
            if(frames.size() < n_frames) {
                frames.push_back(references[i]);
                placed.insert(references[i]);
            }
            else {
                placed.erase(frames[0]);
                frames.erase(frames.begin());
                frames.push_back(references[i]);
                placed.insert(references[i]);
                n_repl++;
            }
        }
    }
	// for(std::vector<int>::reverse_iterator it=frames.rbegin(); it != frames.rend(); it++) std::cout << *it << std::endl;
	return n_repl;
}
